# ipp-tax-and-benefit-tables-converters

Scripts to convert IPP's tax and benefit tables from XLS & XLSX to other formats (YAML, ...)

IPP = [Institut des politiques publiques](http://www.ipp.eu/en/)

Original tax and benefit tables tables, in XLSX format:
- English: http://www.ipp.eu/en/tools/ipp-tax-and-benefit-tables/
- French: http://www.ipp.eu/fr/outils/baremes-ipp/

Generated tax and benefit tables tables, in YAML format:
- [Raw YAML files extracted from XLS spreadsheets](https://git.framasoft.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-yaml-raw)

## Copyright and License

```
These scripts are part of OpenFisca.

OpenFisca -- A versatile microsimulation software
By: OpenFisca Team <contact@openfisca.fr>

Copyright (C) 2011, 2012, 2013, 2014, 2015 OpenFisca Team
https://www.openfisca.fr/

OpenFisca is free software; you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

OpenFisca is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
