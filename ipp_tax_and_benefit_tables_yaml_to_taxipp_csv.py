#! /usr/bin/env python
# -*- coding: utf-8 -*-


# OpenFisca -- A versatile microsimulation software
# By: OpenFisca Team <contact@openfisca.fr>
#
# Copyright (C) 2011, 2012, 2013, 2014, 2015 OpenFisca Team
# http://www.openfisca.fr/
#
# This file is part of OpenFisca.
#
# OpenFisca is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# OpenFisca is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""Convert cleaned YAML files extracted from IPP's tax benefit tables to TaxIPP CSV files.

Clean YAML files:
    https://git.framasoft.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-yaml-clean

Generated CSV files:
    https://git.framasoft.org/french-tax-and-benefit-tables/taxipp-parameters

IPP = Institut des politiques publiques <http://www.ipp.eu/>
"""


from __future__ import division

import argparse
import codecs
import collections
import datetime
import logging
import os
import sys

from biryani import strings
import yaml


app_name = os.path.splitext(os.path.basename(__file__))[0]
log = logging.getLogger(app_name)
year_1960 = datetime.date(1960, 1, 1)


# YAML configuration


def dict_constructor(loader, node):
    return collections.OrderedDict(loader.construct_pairs(node))


yaml.add_constructor(yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG, dict_constructor)


# Functions


def get_item_at_path(value, path):
    for name in path:
        if value is None:
            return None
        value = value.get(name)
    return value


def iter_path_and_taxipp_name_couples(taxipp_name_by_path, path = None):
    if path is None:
        path = []
    for key, value in taxipp_name_by_path.iteritems():
        if isinstance(value, dict):
            for descendant_path, descendant_taxipp_name in iter_path_and_taxipp_name_couples(value,
                    path = path + [key]):
                yield descendant_path, descendant_taxipp_name
        else:
            # Value is a TaxIPP name.
            if strings.slugify(value, separator = u'_') != value.strip(u'_'):
                log.warning(u'  Skipping invalid TaxIPP name: "{}" in "{}"'.format(value, u" / ".join(path + [key])))
                continue
            yield path + [key], value


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--format',
        choices = ['month', 'month-1', 'month-2', 'month-3', 'month-4', 'month-5', 'month-6', 'month-7', 'month-8',
            'month-9', 'month-10', 'month-11', 'month-12', 'year-average'],
        default = 'month', help = 'type of generated rows')
    parser.add_argument('-s', '--source-dir', default = 'yaml-clean',
        help = 'path of source directory containing clean YAML files')
    parser.add_argument('-t', '--target-dir', default = 'taxipp-parameters',
        help = 'path of target directory for generated CSV files')
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = "increase output verbosity")
    args = parser.parse_args()
    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)

    file_system_encoding = sys.getfilesystemencoding()

    if not os.path.exists(args.target_dir):
        os.makedirs(args.target_dir)
    obsolete_target_files_path_encoded = set(
        os.path.join(args.target_dir, filename_encoded)
        for filename_encoded in os.listdir(args.target_dir)
        if filename_encoded.endswith('.csv')
        )

    for source_dir_encoded, directories_name_encoded, filenames_encoded in os.walk(args.source_dir):
        directories_name_encoded.sort()
        row_by_month = {}
        taxipp_names = []
        for filename_encoded in sorted(filenames_encoded):
            if not filename_encoded.endswith('.yaml'):
                continue
            filename = filename_encoded.decode(file_system_encoding)
            sheet_name = os.path.splitext(filename)[0]
            source_file_path_encoded = os.path.join(source_dir_encoded, filename_encoded)
            relative_file_path_encoded = source_file_path_encoded[len(args.source_dir):].lstrip(os.sep)
            relative_file_path = relative_file_path_encoded.decode(file_system_encoding)
            if sheet_name.isupper():
                continue
            assert sheet_name.islower(), sheet_name

            log.info(u'Loading file {}'.format(relative_file_path))
            with open(source_file_path_encoded) as source_file:
                data = yaml.load(source_file)
            taxipp_name_by_path = data.get(u"Noms TaxIPP")
            if taxipp_name_by_path is None:
                log.info(u'  Skipping file {} without "Noms TaxIPP"'.format(relative_file_path))
                continue
            rows = data.get(u"Valeurs")
            if rows is None:
                log.info(u'  Skipping file {} without "Valeurs"'.format(relative_file_path))
                continue

            path_by_taxipp_name = collections.OrderedDict(
                (taxipp_name, path)
                for path, taxipp_name in iter_path_and_taxipp_name_couples(taxipp_name_by_path)
                )
            for taxipp_name in path_by_taxipp_name.iterkeys():
                taxipp_slug = strings.slugify(taxipp_name)
                if taxipp_slug in ('date', 'date-ir', 'date-rev', 'date-revenus', 'jorf', 'note', 'ref-leg',
                        'reference-legislative', 'references-legislatives', 'notes'):
                    continue
                if taxipp_name in taxipp_names:
                    log.warning(u'  TaxIPP name "{}" is used several times for different columns'.format(taxipp_name))
                taxipp_names.append(taxipp_name)

            row_by_start = {}
            for value_index, row in enumerate(rows):
                start = row.get(u"Date d'effet")
                if start is None:
                    for date_name in (
                            u"Age de départ (AAD=Age d'annulation de la décôte)",
                            u"Date",
                            u"Date de perception du salaire",
                            u"Date ISF",
                            ):
                        start = row.get(date_name)
                        if start is not None:
                            break
                    else:
                        # No date found. Skip row.
                        continue
                elif not isinstance(start, datetime.date):
                    start = start[u"Année Revenus"]
                month = start.replace(day = 1)  # Always assume first day of month.
                row_by_start[start] = row

            for month, row in sorted(row_by_start.iteritems()):
                cell_by_taxipp_name = row_by_month.setdefault(month, {})
                for taxipp_name, path in path_by_taxipp_name.iteritems():
                    taxipp_slug = strings.slugify(taxipp_name)
                    if taxipp_slug in ('date', 'date-ir', 'date-rev', 'note', 'ref-leg', 'notes'):
                        continue
                    cell = get_item_at_path(row, path)
                    if cell is not None:
                        if isinstance(cell, basestring):
                            if cell in (u'-', u'na', u'nc'):
                                # Value is unknown. Previous value must be propagated.
                                continue
                            split_cell = cell.split()
                            if len(split_cell) == 2 and split_cell[1] in (
                                    u'%',
                                    u'AF',  # anciens francs
                                    u'CFA',  # francs CFA
                                    u'COTISATIONS',
                                    u'EUR',
                                    u'FRF',
                                    ):
                                cell = float(split_cell[0])
                                unit = split_cell[1]
                                if unit == u'%':
                                    cell = cell / 100
                                elif unit == u'AF':
                                    # Convert "anciens francs" to €.
                                    cell = round(cell / (100 * 6.55957), 2)
                                elif unit == u'FRF':
                                    # Convert "nouveaux francs" to €.
                                    if month < year_1960:
                                        cell /= 100
                                    cell = round(cell / 6.55957, 2)
                            else:
                                # Ignore other strings. Previous value must  be propagated.
                                continue
                        elif not isinstance(cell, (float, int)):
                            # Ignore other strings. Previous value must  be propagated.
                            continue
                        if isinstance(cell, float) and cell == int(cell):
                            cell = int(cell)
                    cell_by_taxipp_name[taxipp_name] = cell

        if not row_by_month:
            continue

        # Remove "columns" without values.
        unused_taxipp_names = set(taxipp_names)
        for row in row_by_month.itervalues():
            for taxipp_name in sorted(unused_taxipp_names):
                cell = row.get(taxipp_name)
                if cell not in (None, 0):
                    unused_taxipp_names.remove(taxipp_name)
        if unused_taxipp_names:
            log.warning(u"Removing empty (or 0 or nc) columns: {}".format(u', '.join(sorted(unused_taxipp_names))))
            for row in row_by_month.itervalues():
                for taxipp_name in unused_taxipp_names:
                    row.pop(taxipp_name, None)
            taxipp_names = [
                taxipp_name
                for taxipp_name in taxipp_names
                if taxipp_name not in unused_taxipp_names
                ]

        # Add missing months and nc cells in months (by repeating the cells of the previous months).
        last_row_line = [None] * len(taxipp_names)
        first_month = min(row_by_month.keys())
        last_month = max(row_by_month.keys())
        row_line_by_month = collections.OrderedDict()
        for year_number in range(first_month.year, last_month.year + 1):
            for month_number in range(1, 13):
                month = datetime.date(year_number, month_number, 1)
                row = row_by_month.get(month, {})
                row_line = []
                for column_index, taxipp_name in enumerate(taxipp_names):
                    cell = row.get(taxipp_name, UnboundLocalError)
                    if cell is UnboundLocalError:
                        cell = last_row_line[column_index]
                    row_line.append(cell)
                row_line_by_month[month] = row_line
                last_row_line = row_line

        # Aggregate lines by year if needed.
        if args.format == 'year-average':
            # Compute yearly average of month values.
            average_row_line_by_month = collections.OrderedDict()
            for year_number in range(first_month.year, last_month.year + 1):
                count_row_line = [0] * len(taxipp_names)
                sum_row_line = [0] * len(taxipp_names)
                for month_number in range(1, 13):
                    for column_index, cell in enumerate(row_line_by_month[datetime.date(year_number,
                            month_number, 1)]):
                        if cell is not None:
                            count_row_line[column_index] += 1
                            sum_row_line[column_index] += cell
                average_row_line_by_month[datetime.date(year_number, 1, 1)] = [
                    None if count == 0 else cell / count
                    for cell, count in zip(sum_row_line, count_row_line)
                    ]
            row_line_by_month = average_row_line_by_month
        elif args.format != 'month':
            # Keep only one month of each year.
            month_number = int(args.format.split('-')[1])
            row_line_by_month = collections.OrderedDict(
                (month, row_line_by_month[month])
                for month in (
                    datetime.date(year_number, month_number, 1)
                    for year_number in range(first_month.year, last_month.year + 1)
                    )
                )

        # Write TaxIPP CSV file.
        target_file_path_encoded = os.path.join(args.target_dir, '{}.csv'.format(os.path.basename(source_dir_encoded)))
        with codecs.open(target_file_path_encoded, 'w', encoding = 'utf-8') as target_file:
            target_file.write(u'date,{}\n'.format(u','.join(taxipp_names)))
            for month, row_line in row_line_by_month.iteritems():
                date_str = unicode(month.year) if args.format == 'year-average' else u'{}-{:02d}'.format(month.year,
                    month.month)
                target_file.write(u'{},{}\n'.format(date_str, u','.join(
                    u'' if cell is None else unicode(cell)
                    for cell in row_line
                    )))
        obsolete_target_files_path_encoded.discard(target_file_path_encoded)

    # Delete obsolete files.
    for obsolete_target_file_path_encoded in obsolete_target_files_path_encoded:
        os.remove(obsolete_target_file_path_encoded)

    return 0


if __name__ == "__main__":
    sys.exit(main())
